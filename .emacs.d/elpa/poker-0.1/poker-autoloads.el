;;; poker-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (poker) "poker" "poker.el" (22012 26924 427105
;;;;;;  834000))
;;; Generated autoloads from poker.el

(autoload 'poker "poker" "\
Play a game of texas hold 'em poker.

\(fn INITIAL-STACK MIN-BET PLAYERS)" t nil)

;;;***

;;;### (autoloads nil nil ("poker-pkg.el") (22012 26924 567357 435000))

;;;***

(provide 'poker-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; poker-autoloads.el ends here
